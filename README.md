The MGI Database is an abstraction layer API built to help communicate with databases from LabVIEW.

**Note:** This package only provides the abstract interface for the database types. See other MGI Database packages for the concrete implementations of ODBC, .NET and other database interface.

## The Database Interface

The Database interface is an abstraction layer used to facility communicating with any database engine. Developers can then create concrete implementations to communicate with a variety of different database engines and (non-LabVIEW) abstraction layers.

## Contributing

See [Contributing.md](CONTRIBUTING.md) for information on how to submit pull requests. Bugs can be reported using the repositories issue tracker.

#### _This package is implemented with LabVIEW 2017_
